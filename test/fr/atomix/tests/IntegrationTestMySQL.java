package fr.atomix.tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.atomix.api.SQLSimpleApi.annotations.Row;
import fr.atomix.api.SQLSimpleApi.annotations.Value;
import fr.atomix.api.SQLSimpleApi.annotations.Where;
import fr.atomix.api.SQLSimpleApi.common.DataType;
import fr.atomix.api.SQLSimpleApi.common.DatabaseUtils;
import fr.atomix.api.SQLSimpleApi.common.KeyInfo;
import fr.atomix.api.SQLSimpleApi.common.Request;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataInsertionException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataRetrieveException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.TableCreationException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.WrongBaseClassException;
import fr.atomix.api.SQLSimpleApi.mysql.MySQLProperties;
import fr.atomix.api.SQLSimpleApi.mysql.MySQLUtils;

public class IntegrationTestMySQL {

	private static DatabaseUtils utils = null;
	
	@Row(tableName = "test_api")
	public static class ContainerId extends Request
	{
		private static final long serialVersionUID = -3556493185093788565L;
		public int nbCalled = 0;
		
		
		@Where(fieldName = "id", type = DataType.INTEGER)
		@Value(fieldName = "id", type = DataType.INTEGER, infos = {KeyInfo.PRIMARY_KEY, KeyInfo.AUTO_INCREMENT})
		public int id;
		
		@Value(fieldName = "field_str", type = DataType.TEXT)
		public String field_str;
		
		@Value(fieldName = "field_value", type = DataType.INTEGER)
		public int field_value;
		
		@Override
		public void postInsert(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException {
			nbCalled += 1;
		}

		@Override
		public void postUpdate(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException {
			nbCalled += 2;
		}

		@Override
		public void postRetrieve(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException {
			nbCalled += 3;
		}
		
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		MySQLProperties props = new MySQLProperties();
		props.user = "root";
		props.password = "root";
		props.setDatabase("test");
		props.setHost("localhost");
		props.setPort(3306);
		utils = new MySQLUtils(props);
		utils.connect();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utils.close();
	}
	
	@Before
	public void setupBeforeTest() throws Exception
	{
		utils.createTable(ContainerId.class);
	}
	
	@After
	public void tearDownAfterTest() throws Exception
	{
		utils.dropTable(ContainerId.class);
	}

	@Test
	public void testDataManipulationId() throws WrongBaseClassException, TableCreationException, DataInsertionException, DataRetrieveException {
		ContainerId id = new ContainerId();
		id.field_str = "StringTest";
		id.field_value = 25;
		utils.insertData(id);
		id.field_str = "SecondTest";
		id.field_value = 12;
		utils.insertData(id);
		id.id = 1;
		id = utils.retrieveData(id);
		assertEquals("StringTest", id.field_str);
		assertEquals(id.field_value, 25);
		id.id = 2;
		id = utils.retrieveData(id);
		assertEquals("SecondTest", id.field_str);
		assertEquals(12, id.field_value);
		List<ContainerId> c = utils.retrieveDataList(ContainerId.class);
		assertEquals(2, c.size());
	}

}
