package fr.atomix.api.SQLSimpleApi.sqlite;

import java.sql.DriverManager;
import java.sql.SQLException;

import fr.atomix.api.SQLSimpleApi.common.DatabaseUtils;
import fr.atomix.api.SQLSimpleApi.common.DynamicPreparedStatement;
import fr.atomix.api.SQLSimpleApi.common.KeyInfo;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.ConnectionException;

/**
 * 
 * @author Atomix
 *
 */
public class SQLiteUtils extends DatabaseUtils {

	private final String dbPath;
	
	public SQLiteUtils(String dbPath) throws ClassNotFoundException
	{
		this(dbPath, new DynamicPreparedStatement());
	}
	
	/**
	 * 
	 * @param dbPath Database path, relative to the server.jar
	 * @throws ClassNotFoundException 
	 */
	public SQLiteUtils(String dbPath, DynamicPreparedStatement builder) throws ClassNotFoundException
	{
		super(builder);
		Class.forName("org.sqlite.JDBC");
		this.dbPath = dbPath;
	}
	
	@Override
	protected void connectToDb() throws ConnectionException {
		try {
			this.con = DriverManager.getConnection("jdbc:sqlite:"+dbPath);
			execStatement = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ConnectionException();
		}
	}

	@Override
	public String getSqlType(KeyInfo info) {
		switch (info)
		{
		case AUTO_INCREMENT:
			return ("AUTOINCREMENT");
		case NOT_NULL:
			return ("NOT NULL");
		case PRIMARY_KEY:
			return ("PRIMARY KEY");
		}
		return (null);
	}

}
