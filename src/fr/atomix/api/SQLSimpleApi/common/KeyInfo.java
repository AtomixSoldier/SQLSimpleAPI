package fr.atomix.api.SQLSimpleApi.common;

import fr.atomix.api.SQLSimpleApi.annotations.Value;

/**
 * 
 * @author Atomix
 *
 */
public enum KeyInfo {

	PRIMARY_KEY,
	NOT_NULL,
	AUTO_INCREMENT;
	
	public static boolean containKeyInfo(Value field, KeyInfo info)
	{
		for (KeyInfo f : field.infos())
		{
			if (f == info)
				return (true);
		}
		return (false);
	}
}
