package fr.atomix.api.SQLSimpleApi.common;

import java.io.Serializable;

import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataRetrieveException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.WrongBaseClassException;

/**
 * 
 * @author Atomix
 *
 */
public abstract class Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8689873763549570631L;

	/**
	 * Called after the data was inserted in database </br>
	 * Can be used to retrieve a auto incremented variable set by the database
	 * @param utils
	 * @throws WrongBaseClassException
	 * @throws DataRetrieveException
	 */
	public abstract void postInsert(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException;

	/**
	 * Called after an update has been done </br>
	 * Should be used to update where clauses if they are not set as Value field
	 * @param utils The database utils
	 * @throws WrongBaseClassException
	 * @throws DataRetrieveException
	 */
	public abstract void postUpdate(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException;

	/**
	 * Called after a retrieve of data </br>
	 * Should update where clauses that are not set as Value
	 * @param utils
	 * @throws WrongBaseClassException
	 * @throws DataRetrieveException
	 */
	public abstract void postRetrieve(DatabaseUtils utils) throws WrongBaseClassException, DataRetrieveException;
}
