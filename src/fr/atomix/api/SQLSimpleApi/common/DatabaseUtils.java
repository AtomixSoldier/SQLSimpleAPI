package fr.atomix.api.SQLSimpleApi.common;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.atomix.api.SQLSimpleApi.annotations.Row;
import fr.atomix.api.SQLSimpleApi.annotations.Value;
import fr.atomix.api.SQLSimpleApi.annotations.Where;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.ConnectionException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataDeleteException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataInsertionException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataRetrieveException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.DataUpdateException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.TableCreationException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.TableDropException;
import fr.atomix.api.SQLSimpleApi.common.UtilsExceptions.WrongBaseClassException;

/**
 *
 * @author Atomix
 *
 */
public abstract class DatabaseUtils {

	protected Connection con = null;
	protected Statement execStatement = null;
	protected final DynamicPreparedStatement statementBuilder;
	
	protected DatabaseUtils(DynamicPreparedStatement statementBuilder)
	{
		this.statementBuilder = statementBuilder;
	}

	protected boolean assertClassIsRow(Class<?> c)
	{
		return (c.getAnnotation(Row.class) != null);
	}

	protected boolean assertClassContainRetrieve(Class<?> c)
	{
		List<Field> fields = DynamicPreparedStatement.getFieldsUpTo(c, Request.class);
		for (Field f : fields)
		{
			if (f.getAnnotation(Where.class) != null)
				return (true);
		}
		return (false);
	}

	public void connect() throws ConnectionException
	{
		this.connectToDb();
		this.statementBuilder.setConnection(con);
	}
	
	public String getSqlType(KeyInfo[] infos)
	{
		String ret = "";
		for (KeyInfo t : infos)
			ret += getSqlType(t)+ " ";
		return (ret.trim());
	}
	public abstract String getSqlType(KeyInfo info);
	
	/**
	 * Connect to the database
	 * @throws ConnectionException
	 */
	protected abstract void connectToDb() throws ConnectionException;

	public void close() {
		try {
			if (execStatement != null && !execStatement.isClosed())
				execStatement.close();
			if (con != null && !con.isClosed())
				con.close();
		} catch (SQLException e) {}
	}

	/**
	 * Create the table linked to the descriptor
	 * @param descriptor The descriptor
	 * @throws WrongBaseClassException
	 * @throws TableCreationException
	 */
	public synchronized void createTable(Class<? extends Request> descriptor) throws WrongBaseClassException, TableCreationException
	{
		if (!this.assertClassIsRow(descriptor))
			throw new UtilsExceptions.WrongBaseClassException();
		try {
			PreparedStatement createStatement = this.statementBuilder.createPreparedStatement(descriptor, this);
			createStatement.executeUpdate();
			createStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new UtilsExceptions.TableCreationException();
		}
	}

	/**
	 * Insert data in the database
	 * @param instance The instance containing data
	 * @throws WrongBaseClassException
	 * @throws TableCreationException
	 * @throws DataInsertionException
	 */
	public synchronized void insertData(Request instance) throws WrongBaseClassException, TableCreationException, DataInsertionException {
		if (!this.assertClassIsRow(instance.getClass()))
			throw new UtilsExceptions.WrongBaseClassException();
		try {
			PreparedStatement insertStatement = this.statementBuilder.insertPreparedStatement(con, instance);
			insertStatement.executeUpdate();
			insertStatement.close();
			instance.postInsert(this);
		} catch (IllegalArgumentException | IllegalAccessException | SQLException | DataRetrieveException | IOException e) {
			e.printStackTrace();
			throw new UtilsExceptions.DataInsertionException();
		}
	}

	/**
	 * Update data in the database, using the where clauses of the instance
	 * @param instance The Request instance
	 * @throws WrongBaseClassException
	 * @throws DataUpdateException
	 */
	public synchronized void updateData(Request instance) throws WrongBaseClassException, DataUpdateException {
		if (!this.assertClassIsRow(instance.getClass()) || !this.assertClassContainRetrieve(instance.getClass()))
			throw new UtilsExceptions.WrongBaseClassException();
		try {
			PreparedStatement updateStatement = this.statementBuilder.updatePreparedStatement(instance);
			updateStatement.executeUpdate();
			updateStatement.close();
			instance.postUpdate(this);
		} catch (IllegalArgumentException | IllegalAccessException | SQLException | DataRetrieveException | IOException e) {
			e.printStackTrace();
			throw new UtilsExceptions.DataUpdateException();
		}
	}

	private static void fillFieldValues(Field f, ResultSet rs, Request ret) throws IllegalArgumentException, IllegalAccessException, SQLException, ClassNotFoundException, IOException
	{
		Value db = f.getAnnotation(Value.class);
		if (db == null)
			return;
		f.setAccessible(true);
		switch (db.type())
		{
		case DOUBLE:
			f.setDouble(ret, rs.getDouble(db.fieldName()));
			break;
		case INTEGER:
			f.setInt(ret, rs.getInt(db.fieldName()));
			break;
		case TEXT:
			f.set(ret, rs.getString(db.fieldName()));
			break;
		case VARCHAR:
			f.set(ret, rs.getString(db.fieldName()));
			break;
		case BLOB:
			Blob b = rs.getBlob(db.fieldName());
			ObjectInput in = new ObjectInputStream(b.getBinaryStream());
			f.set(ret, in.readObject());
			break;
		}
	}

	/**
	 * Retrieve data from the database
	 * @param instance The instance to use
	 * @return Return an object of instance T, T being the final type of instance
	 * @throws WrongBaseClassException
	 * @throws DataRetrieveException
	 */
	@SuppressWarnings("unchecked")
	public synchronized <T> T retrieveData(Request instance) throws WrongBaseClassException, DataRetrieveException {
		if (!this.assertClassIsRow(instance.getClass()) || !this.assertClassContainRetrieve(instance.getClass()))
			throw new UtilsExceptions.WrongBaseClassException();
		Request ret = null;
		try {
			PreparedStatement retrieveStatement = this.statementBuilder.retrievePreparedStatement(instance, true);
			ResultSet rs = retrieveStatement.executeQuery();
			if (!rs.next())
			{
				retrieveStatement.close();
				rs.close();
				return (null);
			}
			ret = instance.getClass().newInstance();
			for (Field f : DynamicPreparedStatement.getFieldsUpTo(instance.getClass(), Request.class))
				DatabaseUtils.fillFieldValues(f, rs, ret);
			retrieveStatement.close();
			rs.close();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SQLException | IOException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new UtilsExceptions.DataRetrieveException();
		}
		ret.postRetrieve(this);
		return (T) (ret);
	}

	public synchronized <T extends Request> List<T> retrieveDataList(Class<T> c) throws WrongBaseClassException
	{
		List<T> ret = new ArrayList<>();
		if (!this.assertClassIsRow(c) || !this.assertClassContainRetrieve(c))
			throw new UtilsExceptions.WrongBaseClassException();
		T add = null;
		try {
			PreparedStatement retrieveStatement = this.statementBuilder.retrievePreparedStatement(c.newInstance(), false);
			ResultSet rs = retrieveStatement.executeQuery();
			while (rs.next())
			{
				add = c.newInstance();
				for (Field f : DynamicPreparedStatement.getFieldsUpTo(c, Request.class))
					fillFieldValues(f, rs, add);
				ret.add(add);
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SQLException | IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return (ret.isEmpty() ? null : ret);
	}

	/**
	 * Delete data from the database where requirement of instance are met
	 * @param instance The instance to use
	 * @throws WrongBaseClassException
	 * @throws DataDeleteException
	 */
	public synchronized void deleteData(Request instance) throws WrongBaseClassException, DataDeleteException {
		if (!this.assertClassIsRow(instance.getClass()) || !this.assertClassContainRetrieve(instance.getClass()))
			throw new UtilsExceptions.WrongBaseClassException();
		try {
			PreparedStatement deleteStatement = this.statementBuilder.deletePreparedStatement(instance);
			deleteStatement.executeUpdate();
		} catch (IllegalArgumentException | IllegalAccessException | SQLException | IOException e) {
			e.printStackTrace();
			throw new UtilsExceptions.DataDeleteException();
		}
	}

	/**
	 * Drop table used by the descriptor
	 * @param decriptor
	 * @throws TableDropException
	 * @throws WrongBaseClassException
	 */
	public synchronized void dropTable(Class<? extends Request> decriptor) throws TableDropException, WrongBaseClassException {
		if (!this.assertClassIsRow(decriptor))
			throw new UtilsExceptions.WrongBaseClassException();
		Row row = decriptor.getAnnotation(Row.class);
		try {
			execStatement.executeUpdate("DROP TABLE IF EXISTS " + row.tableName() + ";");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new UtilsExceptions.TableDropException();
		}
	}
}
