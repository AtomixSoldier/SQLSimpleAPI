package fr.atomix.api.SQLSimpleApi.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import fr.atomix.api.SQLSimpleApi.common.DataType;

/**
 * Used to represent WHERE clauses
 * @author Atomix
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Where {
	public String fieldName();
	public DataType type();
}
