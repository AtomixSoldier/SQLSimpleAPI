package fr.atomix.api.SQLSimpleApi.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Represent a database row
 * @author Atomix
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Row {

	public String tableName();
}
