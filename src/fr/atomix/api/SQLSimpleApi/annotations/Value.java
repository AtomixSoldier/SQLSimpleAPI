package fr.atomix.api.SQLSimpleApi.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import fr.atomix.api.SQLSimpleApi.common.DataType;
import fr.atomix.api.SQLSimpleApi.common.KeyInfo;

/**
 * Represent a table value
 * @author Atomix
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Value {

	public String fieldName();
	public DataType type();
	public int length() default 0;
	public KeyInfo[] infos() default {};
}
